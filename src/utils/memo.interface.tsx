export interface memo {
    title: string,
    color: string,
    year: number,
    month: number,
    date: number,
    startTime: number,
    endTime: number,
    url?: string,
    notes?: string
}