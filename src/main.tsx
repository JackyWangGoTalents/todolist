import * as React from 'react';
import { render } from 'react-dom';
import { AppComponent } from './pages/app';

render(
    <AppComponent />,
    document.body
);